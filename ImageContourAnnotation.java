/**
 * Copyright Robert A. Alexander 2012-2013
 * Released under The MIT License (MIT)
 * See COPYING for details
 */

import java.awt.*;
import java.util.*;

import math.geom2d.*;
import math.geom2d.line.*;

class ImageContourAnnotation extends ImageAnnotation
{
    public LinkedList<Point> contour;
    
    public ImageContourAnnotation(String name, LinkedList<Point> contour, Color color)
    {
        super(name, contour.getFirst(), color);
        this.contour = contour;
    }
    
    public void draw(Graphics g)
    {
        if (this.contour.size() == 0) return;
        
        g.setColor(this.color);
        
        if (this.contour.size() > 1)
        {
            Point last = null;
            for (Point curr : this.contour)
            {
                if (last != null)
                {
                    g.drawLine(last.x, last.y,
                               curr.x, curr.y);
                }
                last = curr;
            }
        }
        else if (this.contour.size() == 1)
        {
            this.point = contour.getFirst();
            super.draw(g);
        }
    }

    protected boolean restrictToLine(Line2D line)
    {
        Point2D last = null;
        for (Point _curr : this.contour)
        {
            Point2D curr = new Point2D(_curr.x, _curr.y);
            if (null != last)
            {
                Point2D intersection = line.intersection(new Line2D(curr, last));
                if (null != intersection)
                {
                    // TODO there may be more than one point here
                    this.contour.clear();
                    this.contour.add(intersection.getAsInt());
                    return true;
                }
            }
            last = curr;
        }
        return false;
    }
    
    public static void test()
    {
        // unit square
        LinkedList<Point> sq = new LinkedList<Point>();
        sq.add(new Point(-1, -1));
        sq.add(new Point(-1,  1));
        sq.add(new Point( 1,  1));
        sq.add(new Point( 1, -1));
        ImageContourAnnotation square = new ImageContourAnnotation("", sq, Color.BLACK);

        // verify what we added
        TestHarness.AssertEqual(new Point(-1, -1), square.contour.get(0));
        TestHarness.AssertEqual(new Point(-1,  1), square.contour.get(1));
        TestHarness.AssertEqual(new Point( 1,  1), square.contour.get(2));
        TestHarness.AssertEqual(new Point( 1, -1), square.contour.get(3));

        Line2D line = new Line2D(new Point2D(0,0), new Point2D(2,0));

        TestHarness.Assert(square.restrictToLine(line));
        TestHarness.AssertEqual(new Point(1,0), square.contour.getFirst());
        TestHarness.AssertEqual(square.contour.size(), 1);
    }
}

