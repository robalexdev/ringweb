/**
 * Copyright Robert A. Alexander 2012-2013
 * Released under The MIT License (MIT)
 * See COPYING for details
 */

import java.io.*;
import java.awt.*;
import javax.swing.*;
import javax.imageio.*;
import java.awt.event.*;

import math.geom2d.*;
import math.geom2d.line.*;

class MouseAnnotate implements MouseListener, MouseMotionListener
{
    private AnnotateImage orig;
    private AnnotateStateTracker state;
    
    private Point lineOfInterestStart;
    private Point lineOfInterestEnd;
    
    public MouseAnnotate(AnnotateImage orig)
    {
        this.orig = orig;
        this.lineOfInterestStart = null;
        this.lineOfInterestEnd = null;
        this.state = new AnnotateStateTracker();
    }
    
    public void mouseClicked(MouseEvent e)
    {
        Point p = new Point(e.getX(), e.getY());
        orig.clearCursor();
        switch (this.state.getState())
        {
            case FINDING_START:
                this.lineOfInterestStart = p;
                break;
            
            case FINDING_END:
                this.lineOfInterestEnd =
                    new Point(e.getX(), this.lineOfInterestStart.y);
                
                if (this.lineOfInterestEnd.x >
                    this.lineOfInterestStart.x)
                {
                    // Keep start on left, end on right
                    // Swap
                    Point t = this.lineOfInterestStart;
                    this.lineOfInterestStart = this.lineOfInterestEnd;
                    this.lineOfInterestEnd = t;
                }
                
                // Remove all "Canny Annotations" that don't fall near this line
                orig.RemoveFilter(new AnnotateImage.Filter() {
                    public boolean check(ImageAnnotation anno)
                    {
                        if (!anno.name.equals("Canny"))
                            return false;
                        return !anno.restrictToLine(
                            new Line2D(
                                lineOfInterestStart.x, lineOfInterestStart.y,
                                lineOfInterestEnd.x, lineOfInterestEnd.y));
                    }
                });
                break;
            
            case FINDING_EDGE:
                // Adjust the point
                p = CalcPos(p);
                ImageAnnotation a =
                    new ImageAnnotation("Click", p, Color.BLACK);
                orig.annotate(a);
                break;
        }
        this.state.nextState();
        orig.repaint();
    }
    
    public void mouseMoved(MouseEvent e)
    {
        Point p = new Point(e.getX(), e.getY());
        switch (this.state.getState())
        {
            case FINDING_START:
                this.lineOfInterestStart = p;
                orig.setCursor(p, Color.GREEN);
                break;
            
            case FINDING_END:
                this.lineOfInterestEnd =
                    new Point(p.x, this.lineOfInterestStart.y);
                orig.setLineOfInterest(this.lineOfInterestStart,
                    this.lineOfInterestEnd);
                orig.setCursor(p, Color.RED);
                break;
            
            case FINDING_EDGE:
                // Adjust the point
                p = CalcPos(p);
                orig.setCursor(p, Color.BLACK);
                break;
        }
        orig.repaint();
    }
    
    public void mouseExited(MouseEvent e){}
    public void mouseEntered(MouseEvent e){}
    public void mouseReleased(MouseEvent e){}
    public void mousePressed(MouseEvent e){}
    public void mouseDragged(MouseEvent e){}
    
    private Point CalcPos(Point p)
    {
        Point s = lineOfInterestStart;
        Point e = lineOfInterestEnd;
        int y = (s.y + e.y) / 2;
        return new Point(p.x, y);
    }
}

