/**
 * Copyright Robert A. Alexander 2012-2013
 * Released under The MIT License (MIT)
 * See COPYING for details
 */

import java.awt.*;

import math.geom2d.*;
import math.geom2d.line.*;

class ImageAnnotation implements Comparable<ImageAnnotation>
{
    public String name;
    public Color color;
    public Point point;
    
    public ImageAnnotation(String name, Point point, Color color)
    {
        this.name = name;
        this.color = color;
        this.point = point;
    }
    
    public void draw(Graphics g)
    {
        g.setColor(this.color);
        // TODO tangent
        g.drawOval(this.point.x, this.point.y, 3, 3);
    }
    
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ImageAnnotation)) {
            return false;
        }
        ImageAnnotation other = (ImageAnnotation) obj;
        return this.point.equals(other.point) &&
               this.name.equals(other.name) &&
               this.color.equals(other.color);
    }
    
    public int hashCode()
    {
        return this.point.hashCode() ^
            this.name.hashCode() ^
            this.color.hashCode();
    }
    
    public int compareTo(ImageAnnotation o)
    {
        int cmp = Integer.compare(this.point.x, o.point.x);
        if (cmp != 0) return cmp;
        cmp = Integer.compare(this.point.y, o.point.y);
        if (cmp != 0) return cmp;
        cmp = this.color.getRGB() - o.color.getRGB();
        if (cmp != 0) return cmp;
        return this.name.compareTo(o.name);
    }
       
    protected boolean restrictToLine(Line2D line)
    {
        return line.contains(new Point2D(this.point.x, this.point.y));
    }
    
    public String toString()
    {
        return name + " " + point;
    }
}

