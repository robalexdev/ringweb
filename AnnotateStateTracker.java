/**
 * Copyright Robert A. Alexander 2012-2013
 * Released under The MIT License (MIT)
 * See COPYING for details
 */

import java.io.*;
import java.awt.*;
import javax.swing.*;
import javax.imageio.*;
import java.awt.event.*;
import java.util.*;

class AnnotateStateTracker
{
    // State engine
    private AnnotateState state;
    
    public AnnotateStateTracker()
    {
        this.state = AnnotateState.FINDING_START;
    }
    
    public AnnotateState getState()
    {
        return this.state;
    }
    
    public void nextState()
    {
        switch (this.state)
        {
            case FINDING_START:
                this.state = AnnotateState.FINDING_END;
                break;
            
            case FINDING_END:
                this.state = AnnotateState.FINDING_EDGE;
                break;
            
            case FINDING_EDGE:
                this.state = AnnotateState.FINDING_EDGE;
                break;
        }
    }
}

