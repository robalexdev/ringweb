/**
 * Copyright Robert A. Alexander 2012-2013
 * Released under The MIT License (MIT)
 * See COPYING for details
 */

import java.io.*;
import java.awt.*;
import javax.swing.*;
import javax.imageio.*;
import java.awt.event.*;
import java.util.LinkedList;
import java.util.regex.*;

public class CannyAnnotate
{
    public CannyAnnotate()
    {
    }
    
    private LinkedList<String> exec(String cmd)
    {
        LinkedList<String> lines = new LinkedList<String>();
        try {
            String line;
            Process p = Runtime.getRuntime().exec(cmd);
            BufferedReader bri = new BufferedReader(
                new InputStreamReader(p.getInputStream()));
            while ((line = bri.readLine()) != null) {
                lines.addLast(line);
            }
            bri.close();
            p.waitFor();
        }
        catch (Exception err) {
            err.printStackTrace();
        }
        return lines;
    }
    
    private LinkedList<LinkedList<Point> > getCountours(String filename)
    {
        LinkedList<String> lines = exec("./canny " + filename);
        
        Pattern pattern = Pattern.compile("\\((\\d+),\\s*(\\d+)\\)");
        
        LinkedList<LinkedList<Point> > contours =
            new LinkedList<LinkedList<Point> >();
        
        LinkedList<Point> currContour = new LinkedList<Point>();
        
        for (String line : lines)
        {
            Matcher matcher = pattern.matcher(line);
            if (!matcher.find())
            {
                if (currContour.size() > 0)
                {
                    contours.add(currContour);
                    currContour = new LinkedList<Point>();
                }
            }
            else
            {
                // 3x zoom, TODO not here...
                int x = Integer.parseInt(matcher.group(1)) * 3;
                int y = Integer.parseInt(matcher.group(2)) * 3;
                currContour.add(new Point(x,y));
            }
        }
        if (currContour.size() > 0)
        {
            contours.add(currContour);
        }
        return contours;
    }
    
    public void cannyAnnotate(String filename, AnnotateImage sample) throws Exception
    {
        LinkedList<LinkedList<Point> > contours = getCountours(filename);
        for (LinkedList<Point> contour : contours)
        {
            sample.annotate(new ImageContourAnnotation(
                "Canny", contour, Color.BLACK));
        }
    }
}

