/**
 * Copyright Robert A. Alexander 2012-2013
 * Released under The MIT License (MIT)
 * See COPYING for details
 */

import java.io.*;
import java.awt.*;
import javax.swing.*;
import javax.imageio.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.*;

class AnnotateImage extends JPanel
{
    // Source image
    protected Image source;
    
    // Annotations
    Point cursor;
    Color cursorColor;
    protected Set<ImageAnnotation> annotatedLocations;
    
    // Line of interest
    Point loiStart;
    Point loiEnd;
    
    public AnnotateImage(Image img)
    {
        // Initialize img here.
        this.source = img;
        this.annotatedLocations = new HashSet<ImageAnnotation>();
        this.loiStart = null;
        this.loiEnd = null;
    }
    
    public void setCursor(Point p, Color color)
    {
        this.cursor = p;
        this.cursorColor = color;
    }
    
    public void setLineOfInterest(Point start, Point end)
    {
        this.loiStart = start;
        this.loiEnd = end;
    }
    
    public void clearCursor()
    {
        this.cursor = null;
    }
    
    public void annotate(ImageAnnotation anno)
    {        
        annotatedLocations.add(anno);
        // TODO Should only need to paint the new mark
        this.repaint();
    }
    
    protected void paintComponent(Graphics g)
    {
        // Zoom 3x, TODO not here...
        g.drawImage(source, 0, 0, source.getWidth(null) * 3, source.getHeight(null) * 3, null);
        
        if (null != this.loiStart && null != this.loiEnd)
        {
            g.setColor(Color.CYAN);
            g.drawLine(
                this.loiStart.x,
                this.loiStart.y,
                this.loiEnd.x,
                this.loiEnd.y);
        }
        
        for (ImageAnnotation a : this.annotatedLocations)
        {
            a.draw(g);
        }
        
        if (null != this.cursor)
        {
            g.setColor(this.cursorColor);
            g.drawRect(
                this.cursor.x-1,
                this.cursor.y-1,
                2,
                2);
        }
        
        g.dispose();
    }

    public Set<ImageAnnotation> getAnnotations()
    {
        return annotatedLocations;
    }
    
    interface Filter {
        abstract boolean check(ImageAnnotation anno);
    }

    public void RemoveFilter(Filter f)
    {
        Iterator<ImageAnnotation> iter =
            this.annotatedLocations.iterator();
        while (iter.hasNext())
        {
            ImageAnnotation anno = iter.next();
            if (f.check(anno))
            {
                // This item is not needed
                iter.remove();
            }
        }
        
    }
}

