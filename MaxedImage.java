/**
 * Copyright Robert A. Alexander 2012-2013
 * Released under The MIT License (MIT)
 * See COPYING for details
 */

import java.io.*;
import java.awt.*;
import javax.swing.*;
import javax.imageio.*;
import java.awt.event.*;
import java.util.*;

class MaxedImage extends AScaledImageComponent
{
    Image img;
    
    public MaxedImage(Image img) throws Exception
    {
        this.img = img;
    }
    
    public void paint(Graphics g)
    {
        int parentW = this.getParent().getWidth();
        int parentH = this.getParent().getHeight();
        int imgW = this.img.getWidth(null);
        int imgH = this.img.getHeight(null);
        int viewW = parentW;
        int viewH = parentH;
        
        // adjust the viewport so the image is compressed but never zoomed
        if (parentH > imgH)
        {
            viewH = imgH;
        }
        
        if (parentW > imgW)
        {
            viewW = imgW;
        }
        
        // Center x, but keep high
        int xOff = (parentW / 2) - (viewW / 2);
        int yOff = 0;
        
        g.drawImage(this.img, xOff, yOff, viewW, viewH, null);
    }
    
    public void zoomIn()
    {
    }
    
    public void zoomOut()
    {
    }
    
    public int GetZoom()
    {
        return -1;
    }
    
    public void pan(Point newCenter)
    {
        // TODO
    }
}

