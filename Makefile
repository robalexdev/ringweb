#
# Copyright Robert A. Alexander 2012-2013
# Released under The MIT License (MIT)
# See COPYING for details
#

CANNY_TARGET = canny

TARGETS = $(CANNY_TARGET)
BASE_OUT_DIR:=_out/
OUT_DIR:=$(BASE_OUT_DIR)/release/c/

CXX = g++

debug: CXX += -DDEBUG -g
debug: build
debug: OUT_DIR:=$(BASE_OUT_DIR)/debug/c/

LIBS = -lopencv_core -lopencv_objdetect -lopencv_highgui -lopencv_imgproc
%: %.cpp
	$(CXX) $^ $(LIBS) -o $(OUT_DIR)/$@

.PHONY: build
build: $(TARGETS)

clean:
	-@rm -rf $(OUT_DIR) 2> /dev/null

