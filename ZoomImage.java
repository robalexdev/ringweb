/**
 * Copyright Robert A. Alexander 2012-2013
 * Released under The MIT License (MIT)
 * See COPYING for details
 */

import java.io.*;
import java.awt.*;
import javax.swing.*;
import javax.imageio.*;
import java.awt.event.*;

class ZoomImage implements ActionListener
{
    public AScaledImageComponent img;
    
    public ZoomImage(AScaledImageComponent img)
    {
        this.img = img;
    }
    
    public void actionPerformed(ActionEvent e)
    {
        try
        {
            if (e.getActionCommand().equals("zoom"))
            {
                this.img.zoomIn();
            }
            else if (e.getActionCommand().equals("zoomOut"))
            {
                this.img.zoomOut();
            }
            else
            {
                System.out.println("Unknown event");
            }
        }
        catch (Exception exception)
        {
            // TODO
        }
    }
}

