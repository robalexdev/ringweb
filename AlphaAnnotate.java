/**
 * Copyright Robert A. Alexander 2012-2013
 * Released under The MIT License (MIT)
 * See COPYING for details
 */

import java.io.*;
import java.awt.*;
import javax.swing.*;
import javax.imageio.*;
import java.awt.event.*;

public class AlphaAnnotate
{
    public static void main(String args[]) throws Exception
    {
        JFrame frame = new JFrame("Image Annotate <prototype>");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container contentPane = frame.getContentPane();
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));

        if (args.length != 1)
        {
            System.err.println("Usage: java AlphaAnnotate <file.png>");
            System.exit(1);
        }
        
        String filename = args[0];
        AnnotateImage sample =
            new AnnotateImage(
                ImageIO.read(
                    new File(filename)));
        
        CannyAnnotate ca = new CannyAnnotate();
        ca.cannyAnnotate(filename, sample);
        
        contentPane.add(sample);
        
        MouseAnnotate mouseListener = new MouseAnnotate(sample);
        sample.addMouseListener(mouseListener);
        sample.addMouseMotionListener(mouseListener);
        
        // Add some buttons
        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.X_AXIS));
        
        JButton exportBtn = new JButton("Export");
        exportBtn.setActionCommand("export");
        exportBtn.addActionListener(new AnnotationExporter(sample));
        buttonPane.add(exportBtn);
        
        /*
        JButton lineOfInterestBtn = new JButton("Mark line of interest");
        lineOfInterestBtn.setActionCommand("line_of_interest");
        lineOfInterestBtn.addActionListener(new LineOfInterest(sample));
        buttonPane.add(lineOfInterestBtn);
        */
        
        contentPane.add(buttonPane);
        
        frame.pack();
        frame.setSize(700, 500);
        frame.setVisible(true);
    }
}

