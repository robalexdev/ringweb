/**
 * Copyright Robert A. Alexander 2012-2013
 * Released under The MIT License (MIT)
 * See COPYING for details
 */

import java.io.*;
import java.awt.*;
import javax.swing.*;
import javax.imageio.*;
import java.awt.event.*;
import java.util.*;

class ZoomableImage extends AScaledImageComponent
{
    protected Point center;
    protected int requestedZoom;
    protected Image img;
    
    public ZoomableImage(Image img, int requestedZoom) throws Exception
    {
        this.requestedZoom = requestedZoom;
        this.center =
            new Point(
                img.getWidth(null) / 2,
                img.getHeight(null) / 2);
        this.img = img;
    }
    
    public void paint(Graphics g)
    {
        // Select a ROI from the source image
        double parentW = this.getParent().getWidth();
        double parentH = this.getParent().getHeight();
        
        double roiW = parentW / this.requestedZoom;
        double roiH = parentH / this.requestedZoom;
        
        int sx1 = (int) (this.center.x - (roiW / 2.0));
        int sy1 = (int) (this.center.y - (roiH / 2.0));
        int sx2 = (int) (this.center.x + (roiW / 2.0));
        int sy2 = (int) (this.center.y + (roiH / 2.0));
        
        int dx1 = 0;
        int dy1 = 0;
        int dx2 = (int) parentW;
        int dy2 = (int) parentH;
        
        g.drawImage(this.img,
                    dx1, dy1, dx2, dy2,
                    sx1, sy1, sx2, sy2,
                    null);
    }
    
    public void zoomIn()
    {
        if (this.requestedZoom < 8)
        {
            this.requestedZoom *= 2;
            this.repaint();
        }
    }
    
    public void zoomOut()
    {
        if (this.requestedZoom >= 2)
        {
            this.requestedZoom /= 2;
            this.repaint();
        }
    }
    
    public int GetZoom()
    {
        return this.requestedZoom;
    }
    
    public void pan(Point newCenter)
    {
        // TODO
    }
}

