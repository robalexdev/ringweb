#!/bin/sh

#
# Copyright Robert A. Alexander 2012-2013
# Released under The MIT License (MIT)
# See COPYING for details
#

java -cp .:javaGeom-0.11.1.jar AlphaAnnotate $1
