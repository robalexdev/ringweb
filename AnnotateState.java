/**
 * Copyright Robert A. Alexander 2012-2013
 * Released under The MIT License (MIT)
 * See COPYING for details
 */

public enum AnnotateState
{
    FINDING_START,
    FINDING_END,
    FINDING_EDGE,
}

