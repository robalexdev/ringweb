/**
 * Copyright Robert A. Alexander 2012-2013
 * Released under The MIT License (MIT)
 * See COPYING for details
 */

import java.io.*;
import java.awt.*;
import javax.swing.*;
import javax.imageio.*;
import java.awt.event.*;

class ZoomedAnnotation extends JPanel // Component?
{
    AScaledImageComponent orig;
    AScaledImageComponent zoom;
    AnnotateImage annotateImg;
    
    public ZoomedAnnotation() throws IOException, Exception
    {
        // Initialize img here.
        Image i = ImageIO.read(new File("sample.png"));
        annotateImg = new AnnotateImage(i);
        orig = new MaxedImage(i); // TODO use the annotated image here
        zoom = new ZoomableImage(i, 2);
    }
    
    public AnnotateImage getAnnotatedImage()
    {
        return annotateImg;
    }
    
    public AScaledImageComponent getOrigComp()
    {
        return orig;
    }
    
    public AScaledImageComponent getZoomComp()
    {
        return zoom;
    }

    public void paintComponent(Graphics g) // TODO why is this class a panel?  Can't we reduce this to main?
    {
        orig.paint(g);
        zoom.paint(g);
    }
    
    public static void main(String[] args) throws IOException, Exception
    {
        JFrame frame = new JFrame("Image Annotate");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container contentPane = frame.getContentPane();
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        
        ZoomedAnnotation za = new ZoomedAnnotation();
        
        // Add the original size image
        AScaledImageComponent orig = za.getOrigComp();
        contentPane.add(orig);
        
        AScaledImageComponent zoom = za.getZoomComp();
        contentPane.add(zoom);
        
        orig.addMouseListener(new PanImage(zoom));
        
        MouseListener annoListener = new MouseAnnotate(za.getAnnotatedImage());
        zoom.addMouseListener(annoListener);
        
        // Add some buttons
        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.X_AXIS));
        JButton zoomBtn = new JButton("Zoom In");
        zoomBtn.setActionCommand("zoom");
        zoomBtn.addActionListener(new ZoomImage(zoom));
        buttonPane.add(zoomBtn);
        JButton zoomOutBtn = new JButton("Zoom Out");
        zoomOutBtn.setActionCommand("zoomOut");
        zoomOutBtn.addActionListener(new ZoomImage(zoom));
        buttonPane.add(zoomOutBtn);
        contentPane.add(buttonPane);
        
        frame.pack();
        frame.setSize(700, 500);
        frame.setVisible(true);
    }
}

