/**
 * Copyright Robert A. Alexander 2012-2013
 * Released under The MIT License (MIT)
 * See COPYING for details
 */

import java.awt.*;
import javax.swing.*;

public abstract class AScaledImageComponent extends Component
{
    public abstract void zoomIn();
    public abstract void zoomOut();
    public abstract int GetZoom();
    public abstract void pan(Point newCenter);
}

