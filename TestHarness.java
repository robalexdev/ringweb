/**
 * Copyright Robert A. Alexander 2012-2013
 * Released under The MIT License (MIT)
 * See COPYING for details
 */

class TestHarness
{    
    protected static boolean issueFound = false;
    protected static void AssertTrace(String message)
    {
        issueFound = true;
        try {
            throw new Exception("ASSERT:" + message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void Assert(boolean v)
    {
        if (!v)
        {
            AssertTrace("false");
        }
    }
    public static void AssertEqual(Object o1, Object o2)
    {
        if (!o1.equals(o2))
        {
            AssertTrace(o1 + " not equal to " + o2);
        }
    }
 
    public static void AssertNull(Object o)
    {
        if (null != o)
        {
            AssertTrace(o + " expected null");
        }
    }

    public static void AssertNonNull(Object o)
    {
        if (null == o)
        {
            AssertTrace(o + " expected non-null");
        }
    }
 
    public static void main(String[] args)
    {
        try {
            AnnotationExporter.test();
            ImageContourAnnotation.test();
        } catch (Exception e) {
            AssertTrace("Unexpected exception.");
            e.printStackTrace();
        }
        if (issueFound)
        {
            System.err.println("Testing failed");
        }
        else
        {
            System.err.println("Testing passed");
        }
        System.exit(issueFound ? 1 : 0);
    }
}
