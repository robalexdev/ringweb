/**
 * Copyright Robert A. Alexander 2012-2013
 * Released under The MIT License (MIT)
 * See COPYING for details
 */

import java.io.*;
import java.awt.*;
import javax.swing.*;
import javax.imageio.*;
import java.awt.event.*;
import java.util.*;

public class AnnotationExporter implements ActionListener
{
    protected AnnotateImage source;
    protected PrintStream out;
    
    public AnnotationExporter(AnnotateImage source)
    {
        this.source = source;
        this.out = System.out;
    }

    public void SetPrintStream(PrintStream out)
    {
        this.out = out;
    }
    
    public void actionPerformed(ActionEvent e)
    {
        // check e
        ArrayList<ImageAnnotation> annotatedLocations =
            new ArrayList<ImageAnnotation>(this.source.getAnnotations());
        Collections.sort(annotatedLocations);
        
        ImageAnnotation last = null;
        for (ImageAnnotation curr : annotatedLocations)
        {
            if (null != last)
            {
                this.out.println(curr.point.distance(last.point));
            }
            last = curr;
        }
    }

    public static void test() throws Exception
    {
        AnnotateImage img = new AnnotateImage(null);
        String expected = "";

        img.annotate(new ImageAnnotation("", new Point(1, 1), Color.BLACK));
        img.annotate(new ImageAnnotation("", new Point(1, 2), Color.BLACK));
        expected += "1.0\n"; // (1,1) -> (1,2)

        img.annotate(new ImageAnnotation("", new Point(3, 2), Color.BLACK));
        expected += "2.0\n"; // (1,2) -> (3,2)

        img.annotate(new ImageAnnotation("", new Point(3, 2), Color.BLACK));
        // Duplicate is ignored
        
        // When sorted, this is the new first
        img.annotate(new ImageAnnotation("", new Point(0, 0), Color.BLACK));
        expected = Math.sqrt(2.0) + "\n" + expected;

        AnnotationExporter exporter = new AnnotationExporter(img);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        exporter.SetPrintStream(ps);
        exporter.actionPerformed(null);
        String result = baos.toString("ASCII");
        TestHarness.AssertEqual(result, expected);
    }
}

