/**
 * Copyright Robert A. Alexander 2012-2013
 * Released under The MIT License (MIT)
 * See COPYING for details
 */

import java.io.*;
import java.awt.*;
import javax.swing.*;
import javax.imageio.*;
import java.awt.event.*;

class PanImage implements MouseListener
{
    public AScaledImageComponent img;
    
    public PanImage(AScaledImageComponent img)
    {
        this.img = img;
    }
    public void mouseClicked(MouseEvent e)
    {
    }
    public void mouseExited(MouseEvent e)
    {
    }
    public void mouseEntered(MouseEvent e)
    {
    }
    public void mouseReleased(MouseEvent e)
    {
    }
    public void mousePressed(MouseEvent e)
    {
    }
    public void mouseMoved(MouseEvent e)
    {        
    }
    public void mouseDragged(MouseEvent e)
    {
    }
}

